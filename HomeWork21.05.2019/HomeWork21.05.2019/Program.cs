﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWork21._05._2019
{
    class Program
    {
        static List<Student> CrateStudents (int count)
        {
            var list = new List<Student>(count);
            var rand = new Random();
            for (int i = 0; i < count; i++)
            {
                list.Add(new Student
                {
                    Name = $"A{i+1}",
                    SurName = $"A{i+1}yan",
                    Age = (byte)rand.Next(18, 50),
                });
            }
            return list;
        }
        static void WriteStudents(List<Student> list, string path)
        {
            using (var sw = new StreamWriter(path))
            {
                foreach (var item in list)
                {
                    sw.WriteLine(item);
                }
            }
        }
        static List<Student> ReadStudents(List<Student> list, string path)
        {
            var srlist = new List<Student>();
            using (var sr=new StreamReader(path))
            {

                var text = "";
                while((text=sr.ReadLine())!=null)
                {
                    srlist.Add(new Student
                    {
                        Name = text.Split(new char[] { ' ' })[0],
                        SurName = text.Split(new char[] { ' ' })[1],
                        Age = Convert.ToByte(text.Split(new char[] { ' ' })[2]),
                    });
                        
                }

            }
            return srlist;
        }
        static void Main(string[] args)
        {
        }
    }
}
